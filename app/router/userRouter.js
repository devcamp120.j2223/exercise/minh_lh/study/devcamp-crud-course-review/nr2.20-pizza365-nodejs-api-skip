// khởi tạo bộ thư viện
const express = require('express');
// import module
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById, getAllLimit, getAllSkip, getAllSort, getAllSkipLimit, getAllSortSkipLimit } = require('../controller/userController')
//khởi tạo router
const router = express.Router();
// create new user
router.post("/users", createUser);
// get all user
router.get("/users", getAllUser)
// get user by id
router.get("/users/:userId", getUserById)
// get user update by id
router.put("/users/:userId", updateUserById)
// delate user
router.delete("/users/:userId", deleteUserById)
// get limit api
router.get("/limit-users", getAllLimit);
// get skip api
router.get("/skip-users", getAllSkip);
// sort user
router.get("/sort-users", getAllSort);
//skip-limit-users
router.get("/skip-limit-users", getAllSkipLimit);
//sort-skip-limit-users
router.get("/sort-skip-limit-users", getAllSortSkipLimit);
// export dữ liệu thành 1 module
module.exports = router;